# ansible-playbooks

Playbooks themselves are at the top level, because that appears to be the only
way to get ansible to work without further configuration.

The playbooks, inventories, and group_vars are named after terraform
environments.

## Usage

1. Clone the infrastructure-playground-secrets repository to the root of this
   repository.
1. Obtain the ansible-vault password, and write it to `ansible_vault_password`
   in this directory.
1. Follow environment-specific instructions below.

## Environments

### vpn

Set the same `HCLOUD_TOKEN` envvar as in the terraform environment of the same
name.

```
ansible-playbook --vault-password-file=ansible_vault_password -i inventories/vpn ./vpn.yml
```

On the first run, you'll need to pass `-e ansible_user=root` as user accounts
will not exist.

### kube_shed

Set the same `HCLOUD_TOKEN` envvar as in the terraform environment of the same
name.

```
ansible-playbook --vault-password-file=ansible_vault_password -i inventories/kube_shed ./kube_shed.yml
```

On the first run, you'll need to pass `-e ansible_user=root` as user accounts
will not exist.
