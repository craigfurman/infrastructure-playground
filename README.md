# infrastructure-playground

A monorepo for infrastructure code (mostly terraform and ansible) for personal
projects and tests. Terraform modules and ansible roles should be reasonably
generic, but this repository also contains concrete environment/host
configuration that references these modules.

Code will be moved in from other projects:
https://gitlab.com/craigfurman/ansible-home,
https://gitlab.com/craigfurman/kube-shed, and
https://gitlab.com/craigf/gitlab-sandbox.
