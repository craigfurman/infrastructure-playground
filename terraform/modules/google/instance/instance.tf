resource "google_compute_instance" "default" {
  count = var.instance_count

  name = format("%s-%s", var.service, module.instance_id.ids[count.index])

  allow_stopping_for_update = var.allow_stopping_for_update
  can_ip_forward            = var.can_ip_forward
  machine_type              = var.machine_type
  tags                      = var.tags
  zone                      = data.google_compute_zones.available_zones.names[count.index]

  dynamic "attached_disk" {
    for_each = var.data_disk_size > 0 ? [0] : []
    content {
      source      = google_compute_disk.data[count.index].self_link
      device_name = "data"
    }
  }

  boot_disk {
    initialize_params {
      image = var.boot_disk_image
      size  = var.boot_disk_size
      type  = var.boot_disk_type
    }
  }

  network_interface {
    subnetwork = var.subnetwork_link
    network_ip = var.static_private_ip ? google_compute_address.static_private_ip[count.index].address : ""

    dynamic "access_config" {
      for_each = var.public_ip == "EPHEMERAL" || var.public_ip == "STATIC" ? [0] : []
      content {
        nat_ip = var.public_ip == "STATIC" ? google_compute_address.static_public_ip[count.index].address : ""
      }
    }
  }

  labels = {
    deployment = module.deployment_id.ids[0]
    origin     = "terraform"
    service    = var.service
  }

  metadata = {
    data-disk-mountpoint = var.data_disk_mountpoint
    data-disk-name       = "data"
    ssh-keys             = var.ssh_keys
    startup-script       = data.local_file.startup_script.content
  }

  scheduling {
    automatic_restart = ! var.preemptible
    preemptible       = var.preemptible
  }
}

resource "google_compute_disk" "data" {
  count = var.data_disk_size > 0 ? var.instance_count : 0

  name = format("%s-%s-data", var.service, module.instance_id.ids[count.index])
  size = var.data_disk_size
  type = var.data_disk_type
  zone = data.google_compute_zones.available_zones.names[count.index]

  labels = {
    deployment = module.deployment_id.ids[0]
    origin     = "terraform"
    service    = var.service
  }
}

# TODO add labels when field out of beta:
# https://www.terraform.io/docs/providers/google/r/compute_address.html#labels
resource "google_compute_address" "static_private_ip" {
  count = var.static_private_ip ? var.instance_count : 0

  address_type = "INTERNAL"
  name         = format("%s-%s-private", var.service, module.instance_id.ids[count.index])
  region       = var.region
  subnetwork   = var.subnetwork_link
}

resource "google_compute_address" "static_public_ip" {
  count = var.public_ip == "STATIC" ? var.instance_count : 0

  address_type = "EXTERNAL"
  name         = format("%s-%s", var.service, module.instance_id.ids[count.index])
  region       = var.region
}

module "instance_id" {
  source = "../../random/id"

  id_count      = var.instance_count
  use_pet_names = var.use_pet_names
}

module "deployment_id" {
  source = "../../random/id"

  id_count      = 1
  use_pet_names = true
}

data "local_file" "startup_script" {
  filename = "${path.module}/bootstrap.sh"
}

data "google_compute_zones" "available_zones" {
  region = var.region
}
