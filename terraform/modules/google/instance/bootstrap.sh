#!/usr/bin/env bash
set -euo pipefail

# For now, the entire purpose of this script is to mount a persistent disk, if
# one is configured. If more functionality is added then this should be modified
# to continue and do other things even if a persistent disk is not configured.
main() {
  device_name="$(get_metadata data-disk-name)"
  device_file="/dev/disk/by-id/google-${device_name}"
  mountpoint="$(get_metadata data-disk-mountpoint)"

  if grep -q "${mountpoint}" /etc/fstab; then
    echo "persistent disk already mounted"
    return 0
  fi

  if [ ! -b "${device_file}" ]; then
    "${device_file} is not a block device, aborting"
    return 1
  fi

  if [[ $(file -sL "${device_file}") != *Linux* ]]; then
    echo "formatting ${device_file}"
    mkfs.ext4 -m 0 -E lazy_itable_init=0,lazy_journal_init=0 "${device_file}" || true
  fi

  mkdir -p "${mountpoint}"
  echo "mounting ${device_file} at ${mountpoint}"
  echo "UUID=$(sudo blkid -s UUID -o value "${device_file}") ${mountpoint} ext4 defaults,nofail 0 2" >>/etc/fstab
  mount --all
}

get_metadata() {
  status=$(curl -s -o /dev/null -w '%{http_code}' -H 'Metadata-Flavor: Google' \
    "http://metadata.google.internal/computeMetadata/v1/instance/attributes/${1}")
  if [ "${status}" -ne 200 ]; then
    echo "metadata key ${1} not found, aborting" >&2
    return 1
  fi

  curl -s -H 'Metadata-Flavor: Google' \
    "http://metadata.google.internal/computeMetadata/v1/instance/attributes/${1}"
}

main
