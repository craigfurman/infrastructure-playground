variable "instance_count" {
  type = number
}

variable "boot_disk_image" {
  type = string
}

variable "boot_disk_type" {
  type    = string
  default = "pd-standard"
}

variable "boot_disk_size" {
  type    = number
  default = 20
}

variable "data_disk_type" {
  type    = string
  default = "pd-standard"
}

# Set to non-zero to provision data disk
variable "data_disk_size" {
  type    = number
  default = 0
}

variable "data_disk_mountpoint" {
  type    = string
  default = "/data"
}

variable "tags" {
  type    = list(string)
  default = []
}

variable "static_private_ip" {
  type    = bool
  default = false
}

variable "public_ip" {
  type        = string
  default     = "NONE"
  description = "NONE, EPHEMERAL, or STATIC."
}

variable "preemptible" {
  type    = bool
  default = false
}

variable "use_pet_names" {
  type        = bool
  default     = false
  description = "use memorable names for instances instead of IDs"
}

variable "allow_stopping_for_update" {
  type    = bool
  default = true
}

variable "can_ip_forward" {
  type    = bool
  default = false
}

variable "machine_type" {}
variable "region" {}
variable "service" {}
variable "ssh_keys" {}
variable "subnetwork_link" {}
