output "static_public_ips" {
  value = google_compute_address.static_public_ip.*.address
}

output "instance_links" {
  value = google_compute_instance.default.*.self_link
}
