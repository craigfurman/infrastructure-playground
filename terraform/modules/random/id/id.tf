resource "random_id" "id" {
  count       = var.use_pet_names ? 0 : var.id_count
  byte_length = var.length
}

resource "random_pet" "id" {
  count  = var.use_pet_names ? var.id_count : 0
  length = var.length
}
