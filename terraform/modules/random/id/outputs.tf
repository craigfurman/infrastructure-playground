output "ids" {
  value = var.use_pet_names ? random_pet.id.*.id : random_id.id.*.hex
}
