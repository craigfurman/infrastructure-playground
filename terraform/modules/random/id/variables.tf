variable "id_count" {
  type = number
}

variable "use_pet_names" {
  type        = bool
  default     = false
  description = "use memorable names instead of random strings"
}

variable "length" {
  type        = number
  default     = 2
  description = "bytes for IDs, words for pets"
}
