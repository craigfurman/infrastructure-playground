resource "hcloud_server" "server" {
  count = var.server_count

  image       = var.image
  location    = var.location == "" ? element(data.hcloud_locations.locs.names, count.index) : var.location
  name        = format("%s-%s", var.service, module.server_id.ids[count.index])
  server_type = var.machine_type
  user_data   = local.user_data

  labels = {
    service    = var.service
    deployment = module.deployment_id.ids[0]
    origin     = "terraform"
  }

  ssh_keys = data.hcloud_ssh_keys.ssh_keys.ssh_keys.*.name
}

resource "hcloud_server_network" "network_membership" {
  count = var.server_count

  network_id = var.network_id
  server_id  = hcloud_server.server[count.index].id
}

module "server_id" {
  source = "../../random/id"

  id_count      = var.server_count
  use_pet_names = var.use_pet_names
}

module "deployment_id" {
  source = "../../random/id"

  id_count      = 1
  use_pet_names = true
}

data "hcloud_locations" "locs" {}

# All SSH keys
data "hcloud_ssh_keys" "ssh_keys" {}

locals {
  user_data = <<-EOS
  #cloud-config
  runcmd: |
    # Not yet used
  EOS
}
