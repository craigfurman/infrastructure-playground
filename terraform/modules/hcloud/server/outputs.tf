output "public_ips" {
  value = hcloud_server.server.*.ipv4_address
}

output "private_ips" {
  value = hcloud_server_network.network_membership.*.ip
}
