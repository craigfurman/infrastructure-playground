variable "server_count" {
  type = number
}

variable "service" {
  type = string
}

variable "network_id" {
  type = string
}

variable "machine_type" {
  type = string
}

variable "image" {
  type = string
}

variable "location" {
  type        = string
  default     = ""
  description = "if unspecified, round-robins the instances around all locations"
}

variable "use_pet_names" {
  type    = bool
  default = false
}
