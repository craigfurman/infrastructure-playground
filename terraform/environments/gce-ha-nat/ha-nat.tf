terraform {
  backend "gcs" {
    bucket = "cfurman-gitlab-sandbox-terraform-state"
    prefix = "gce-ha-nat"
  }
}

provider "google" {
  version = "~> 3.9"
  project = "cfurman-gitlab-sandbox"
}

provider "random" {
  version = "~> 2.2"
}

provider "local" {
  version = "~> 1.4"
}

resource "google_compute_network" "nat_net" {
  name = "nat-net"
}

resource "google_compute_subnetwork" "nat_subnet" {
  name          = "nat-subnet"
  ip_cidr_range = "10.0.0.0/16"
  region        = var.region
  network       = google_compute_network.nat_net.self_link
}

module "nat_gw" {
  source = "../../modules/google/instance"

  boot_disk_image = data.google_compute_image.ubuntu.self_link
  can_ip_forward  = true
  instance_count  = 2
  machine_type    = "f1-micro"
  preemptible     = true
  public_ip       = "STATIC"
  region          = var.region
  service         = "nat"
  ssh_keys        = var.ssh_keys
  subnetwork_link = google_compute_subnetwork.nat_subnet.self_link
  tags            = ["nat"]
}

resource "google_compute_route" "internet_nat" {
  count = length(module.nat_gw.instance_links)

  dest_range        = "0.0.0.0/0"
  name              = format("internet-nat-%02d", count.index + 1)
  network           = google_compute_network.nat_net.self_link
  next_hop_instance = module.nat_gw.instance_links[count.index]

  # Must be higher priority (lower number) than the default internet route.
  priority = 999

  # If not set, this breaks direct ssh to the NAT hosts themselves. I have not
  # tested this using a bastion ssh setup, so it might still be viable.
  tags = ["needs-nat"]
}

module "instance_behind_nat" {
  source = "../../modules/google/instance"

  boot_disk_image = data.google_compute_image.ubuntu.self_link
  instance_count  = 1
  machine_type    = "f1-micro"
  preemptible     = true
  region          = var.region
  service         = "none"
  ssh_keys        = var.ssh_keys
  subnetwork_link = google_compute_subnetwork.nat_subnet.self_link
  tags            = ["needs-nat"]
}

resource "google_compute_firewall" "allow_ssh" {
  name    = "allow-ssh"
  network = google_compute_network.nat_net.self_link

  allow {
    protocol = "tcp"
    ports    = [22]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "allow_icmp" {
  name    = "allow-icmp"
  network = google_compute_network.nat_net.self_link

  allow {
    protocol = "icmp"
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "allow_internal" {
  name    = "allow-internal"
  network = google_compute_network.nat_net.self_link

  allow {
    protocol = "tcp"
  }

  allow {
    protocol = "udp"
  }

  source_ranges = ["10.0.0.0/8"]
}

data "google_compute_image" "ubuntu" {
  family  = "ubuntu-1804-lts"
  project = "gce-uefi-images"
}
