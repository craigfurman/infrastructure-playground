# kube-shed

A kubernetes playground hosted in Hetzner Cloud.

This setup, and the associated Ansible configuration, follows
https://github.com/hobby-kube/guide quite closely.

There is only 1 terraform workspace (default). Set the `HCLOUD_TOKEN` envvar to
use.
