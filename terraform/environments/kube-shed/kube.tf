terraform {
  backend "gcs" {
    bucket = "craigf-terraform-state"
    prefix = "kube-shed"
  }
}

provider "hcloud" {
  version = "~> 1.15"
}

provider "random" {
  version = "~> 2.2"
}

resource "hcloud_network" "default" {
  ip_range = "10.0.0.0/20"
  name     = "default"

  labels = {
    origin = "terraform"
  }
}

resource "hcloud_network_subnet" "default" {
  ip_range     = "10.0.0.0/20"
  network_id   = hcloud_network.default.id
  network_zone = "eu-central"
  type         = "server"
}

module "k8s_master" {
  source = "../../modules/hcloud/server"

  image        = "ubuntu-18.04"
  machine_type = "cx11"
  network_id   = hcloud_network.default.id
  server_count = 1
  service      = "k8s-master"
}

module "k8s_worker" {
  source = "../../modules/hcloud/server"

  image        = "ubuntu-18.04"
  machine_type = "cx11"
  network_id   = hcloud_network.default.id
  server_count = 2
  service      = "k8s-worker"
}
