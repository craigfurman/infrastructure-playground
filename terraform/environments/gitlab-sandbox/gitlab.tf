terraform {
  backend "gcs" {
    bucket = "cfurman-gitlab-sandbox-terraform-state"
    prefix = "gitlab-sandbox"
  }
}

provider "google" {
  version = "~> 3.9"
  project = "cfurman-gitlab-sandbox"
}

provider "cloudflare" {
  version = "~> 2.3.0"
}

provider "random" {
  version = "~> 2.2"
}

provider "local" {
  version = "~> 1.4"
}

module "gitlab_with_geo" {
  source = "../../modules/google/instance"

  boot_disk_image      = data.google_compute_image.ubuntu.self_link
  data_disk_mountpoint = "/var/opt/gitlab"
  data_disk_size       = 20
  instance_count       = 2
  machine_type         = "n1-standard-2"
  preemptible          = true
  region               = var.region
  service              = "gitlab"
  ssh_keys             = var.ssh_keys
  static_private_ip    = true
  public_ip            = "STATIC"
  subnetwork_link      = data.google_compute_subnetwork.default.self_link
  tags                 = ["gitlab"]
  use_pet_names        = true
}

resource "google_compute_firewall" "gitlab" {
  allow {
    protocol = "tcp"
    ports    = [22, 80, 443]
  }

  name          = "gitlab"
  network       = "default"
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["gitlab"]
}

resource "cloudflare_record" "primary" {
  name    = "primary.gitlab-sandbox"
  ttl     = 300
  type    = "A"
  value   = module.gitlab_with_geo.static_public_ips[0]
  zone_id = data.cloudflare_zones.craigfurman_com.zones[0].id
}

resource "cloudflare_record" "geo" {
  name    = "geo.gitlab-sandbox"
  ttl     = 300
  type    = "A"
  value   = module.gitlab_with_geo.static_public_ips[1]
  zone_id = data.cloudflare_zones.craigfurman_com.zones[0].id
}

data "cloudflare_zones" "craigfurman_com" {
  filter {
    name = "craigfurman.com"
  }
}

data "google_compute_subnetwork" "default" {
  name   = "default"
  region = var.region
}

data "google_compute_image" "ubuntu" {
  family  = "ubuntu-1804-lts"
  project = "gce-uefi-images"
}
