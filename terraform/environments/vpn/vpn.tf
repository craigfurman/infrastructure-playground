terraform {
  backend "gcs" {
    bucket = "craigf-terraform-state"
    prefix = "vpn"
  }
}

provider "hcloud" {
  version = "~> 1.15"
}

provider "cloudflare" {
  version = "~> 2.3.0"
}

provider "random" {
  version = "~> 2.2"
}

resource "hcloud_network" "default" {
  ip_range = "10.0.0.0/20"
  name     = "vpn"

  labels = {
    origin = "terraform"
  }
}

resource "hcloud_network_subnet" "default" {
  ip_range     = "10.0.0.0/20"
  network_id   = hcloud_network.default.id
  network_zone = "eu-central"
  type         = "server"
}

module "vpn" {
  source = "../../modules/hcloud/server"

  image         = "ubuntu-18.04"
  machine_type  = "cx11"
  network_id    = hcloud_network.default.id
  server_count  = 1
  service       = "vpn"
  use_pet_names = true
}

resource "cloudflare_record" "vpn" {
  name    = "vpn"
  ttl     = 300
  type    = "A"
  value   = module.vpn.public_ips[0]
  zone_id = data.cloudflare_zones.craigfurman_com.zones[0].id
}

data "cloudflare_zones" "craigfurman_com" {
  filter {
    name = "craigfurman.com"
  }
}
