# vpn

A WireGuard VPN and pihole DNS server hosted in Hetzner Cloud.

There is only 1 terraform workspace (default). Set the `HCLOUD_TOKEN` envvar to
use.
