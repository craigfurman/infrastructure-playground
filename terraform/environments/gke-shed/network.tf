resource "google_compute_network" "default" {
  name                    = "gke-shed"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "default" {
  name          = "gke-shed"
  ip_cidr_range = "10.0.0.0/22"
  region        = var.region
  network       = google_compute_network.default.self_link

  secondary_ip_range {
    range_name    = "k8s-pods"
    ip_cidr_range = "172.16.0.0/20"
  }

  secondary_ip_range {
    range_name    = "k8s-services"
    ip_cidr_range = "172.16.16.0/20"
  }
}
