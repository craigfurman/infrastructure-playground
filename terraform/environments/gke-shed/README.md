# gke-shed

GKE sandbox environment.

There are 2 branch/workspace pairs for this module: master/default, and
cross-cluster-traffic/cross-cluster-traffic. The latter is a short-lived spike
for https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10539.
