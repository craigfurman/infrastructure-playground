terraform {
  backend "gcs" {
    bucket = "cfurman-gitlab-sandbox-terraform-state"
    prefix = "gke-shed"
  }
}

provider "google" {
  version = "~> 3"
  project = "cfurman-gitlab-sandbox"
}

provider "random" {
  version = "~> 2"
}

resource "google_container_cluster" "default" {
  name = random_pet.cluster_name.id

  location   = var.region
  subnetwork = google_compute_subnetwork.default.self_link
  network    = google_compute_network.default.self_link

  min_master_version = "1.16.12-gke.3"

  // Alpha clusters can't be upgraded, which is something we might want to test.
  enable_kubernetes_alpha = false

  // Incredibly, there must be an initial node pool defined, even if it's
  // immediately removed.
  remove_default_node_pool = true
  initial_node_count       = 1

  ip_allocation_policy {
    cluster_secondary_range_name  = "k8s-pods"
    services_secondary_range_name = "k8s-services"
  }

  // No Google, I don't want Stackdriver enabled by default.
  logging_service    = "none"
  monitoring_service = "none"

  // Disable basic auth and client cert auth. Only use IAM auth.
  master_auth {
    username = ""
    password = ""
    client_certificate_config {
      issue_client_certificate = false
    }
  }

  resource_labels = {
    origin     = "terraform"
    service    = "gke"
    deployment = random_pet.cluster_name.id
  }

  # Uncomment for private cluster config
  # private_cluster_config {
  #   enable_private_nodes    = false
  #   enable_private_endpoint = false
  #   master_ipv4_cidr_block  = "192.168.0.0/28"
  # }
}

resource "google_container_node_pool" "default" {
  name_prefix = substr(random_pet.cluster_name.id, 0, 10)
  cluster     = google_container_cluster.default.name
  location    = var.region
  node_count  = 1 // per zone
  version     = "1.16.12-gke.3"

  node_config {
    machine_type = "n1-standard-1"
    preemptible  = true
    tags         = ["gke-shed"]

    labels = {
      origin     = "terraform"
      service    = "gke"
      deployment = random_pet.cluster_name.id
    }
  }
}

resource "random_pet" "cluster_name" {
}
